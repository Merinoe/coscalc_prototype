### About CosCalc ###

This is simple text-based prototype for a budgeting tools that cosplayers can use to assist in tracking their budget for costume and props. Currently, projects can be created, accessed, modified, saved, and loaded.

### How do I get set up? ###

Any compiler should be able to run this code. Thus far, this has only been tested on Eclipse Juno.
