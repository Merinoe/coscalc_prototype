package cosplayBudgeter;

import java.util.HashMap;
import cosplayBudgeter.InvalidCosplayNameException;
import cosplayBudgeter.InvalidFieldException;

/**
 *  A class representing a simple adjustable information storage
 * 	for a single cosplay project. 
 * 
 * @author	Merinoe 
 *
 */
public class Cosplay {
	/*
	 * Rep Invariant:	CharacterName is not null or an empty string. All
	 * 					mapped values are neither empty nor null strings mapped to
	 * 					valid prices (numerical values greater or equal to zero).
	 */
	
	//Cosplay fields
	private String characterName;
	private HashMap<String, Double> costInformation;
	private double currentTotal;
	private int type;
	
	//Grammar Tags
	private static final String openTag = "<cosplay>";
	private static final String closeTag = "</cosplay>";
	private static final String openNameTag = "<name>";
	private static final String closeNameTag = "</name>";
	private static final String openFieldTag = "<field>";
	private static final String closeFieldTag = "</field>";
	private static final String openFieldNTag = "<fname>";
	private static final String closeFieldNTag = "</fname>";
	private static final String openTypeTag = "<type>";
	private static final String closeTypeTag = "</type>";
	private static final String openAmountTag = "<amount>";
	private static final String closeAmountTag = "</amount>";
	
	
	/**
	 * 
	 * @param cosplayName
	 * 										The name of the cosplay.
	 * @throws InvalidCosplayNameException 
	 * 										If the name of the cosplay is
	 * 										an empty string or null.
	 */			
	public Cosplay(String cosplayName) throws InvalidCosplayNameException{
		if(!checkValidString(cosplayName.trim())){
			throw new InvalidCosplayNameException(); 
		}
		
		characterName = cosplayName.trim(); //After confirming validity, create a new cosplay project.
		costInformation = new HashMap<String, Double>();
		currentTotal = 0;
		
	}
	
	/*
	 * Method	:	checkValidString
	 * Param(s)	:	stringToCheck - the string to check for validity.
	 * Returns	:	true of the string is neither null nor empty, false otherwise.
	 * Purpose	:	checks to determine if a cosplay name of field string fulfills the
	 * 				rep invariant.
	 */
	private boolean checkValidString(String stringToCheck){
		return (!stringToCheck.isEmpty() && stringToCheck != null);
	}
	
	/**
	 * Adds a new information field to a Cosplay object, along with its price.
	 * 
	 * @param field
	 * 									A brief description of the material/item to be added.
	 * @param cost			
	 * 									the price of the material/item rounded to 2 decimal places.
	 * @throws InvalidFieldException 
	 * 									If the field already exists, is blank, or null.
	 */
	public void addInformationField(String field, double cost) throws InvalidFieldException{
		//editInformationField should be used instead if an existing field is to be modified.
		if(costInformation.containsKey(field.trim()) || field.isEmpty() || field == null){
			throw new InvalidFieldException(); 
			
		}
		
		costInformation.put(field.trim(), round(cost)); //Save the information if it's a new field.
		recalculateTotal();
	}
	
	
	/**
	 * Replaces the cost of an existing information field with a new cost.
	 * 
	 * @param field
	 * 									the field to edit.
	 * @param cost
	 * 									the new cost value of this field.
	 * @throws InvalidFieldException
	 * 									if the field does not exist. 
	 */
	public void editInformationField(String field, double cost) throws InvalidFieldException{
		if(!costInformation.containsKey(field)){
			throw new InvalidFieldException(); 
		}
		
		costInformation.put(field,  round(cost)); //Update the information if it already exists.
		recalculateTotal(); //Determine the new total cost.
	}
	
	/**
	 * Adds an specified amount to an existing amount in an existing information field.
	 * 
	 * @param field
	 * 									the field to edit.
	 * @param cost
	 * 									the cost to add to the current sum of costs in the field.
	 * @throws InvalidFieldException 
	 * 		 							if the field does not exist.
	 */
	public void addToExistingValue(String field, double cost) throws InvalidFieldException{
		if(!costInformation.containsKey(field.trim())){
			throw new InvalidFieldException(); 
		}
		
		double currentValue = costInformation.get(field);
		currentValue += round(cost);		
		
		costInformation.put(field.trim(), currentValue); //Store the updated information.
		recalculateTotal();
	}

	/**
	 * Deletes the specified field.
	 * <p>Note: the user is not warned
	 * before the field is deleted.</p>
	 * 
	 * @param field
	 * 									the field to remove
	 * @throws InvalidFieldException 
	 * 									if the field does not exist.
	 */
	public void deleteField(String field) throws InvalidFieldException{
		if(!costInformation.containsKey(field.trim())){
			throw new InvalidFieldException(); 
		}
		
		costInformation.remove(field.trim());
		recalculateTotal(); //Determine and store the new sum.
	}
	
	/**
	 * Calculates the total cost of this cosplay.
	 * 
	 * @return
	 * 			the total cost of this cosplay.
	 */
	public double retrieveTotal(){
		return currentTotal;
	}
	
	/*
	 * Method	:	recalculateTotal
	 * Purpose	:	recalculates the total cost of a cosplay and saves it in currentTotal.
	 */
	private void recalculateTotal(){
		double currentSum = 0;
		
		for(double costPerMaterial: costInformation.values()){
			currentSum += costPerMaterial;
		}
		
		currentTotal = currentSum;
	}
	
	/*
	 * Method	:	round
	 * Param(s)	:	valueToRound - the numerical value to round to two decimal places.
	 * Return	: 	the provided value, rounded to two decimal places
	 * Purpose	:	rounds a given value to a valid price.
	 */
	private double round(double valueToRound){
		return Math.round(valueToRound*100.0)/100.0;
	}
	
	/**
	 * Prints a summary of this cosplay's details, consisting of 
	 * the cosplay's name,fields and their respective amounts, 
	 * and total cost.
	 */
	public void printSummary(){
		System.out.println("Cosplay:\t" + characterName);
		
		for(String field: costInformation.keySet()){
			System.out.println(field + ":\t" + costInformation.get(field));
		}
		
		System.out.println("Total:\t" + currentTotal);
	}


	/**
	 * Obtains the name of the cosplay.
	 * 
	 * @return
	 * 			the name of this cosplay.
	 */
	public String retrieveName() {
		return characterName;
	}
	
	/**
	 * Replaces the name of a field with a new name.
	 * 
	 * @param oldField
	 * 									the field to rename.
	 * @param newField
	 * 									the new name of the field.
	 * @throws InvalidFieldException
	 * 									if the new field already exists, is blank, or 
	 * 									null OR if the old field does not exist.
	 */
	public void renameField(String oldField, String newField) throws InvalidFieldException{
		//Check if the field exists.
		if(!costInformation.containsKey(oldField.trim())){
			throw new InvalidFieldException();
		}
		
		double value = costInformation.get(oldField.trim());
		
		addInformationField(newField.trim(), value); //An exception will be thrown if the new field name already exists, is blank, or null.
		costInformation.remove(oldField.trim());
	}
	
	
	/**
	 * Replaces the current name of the cosplay with a new name.
	 * @param library
	 * 										the library this cosplay is contained in.
	 * @param newName
	 * 										the new name of this cosplay.
	 * @throws InvalidCosplayNameException
	 * 										if the new name provided is not null, empty, or already exists.
	 */
	public void renameCosplay(CosplayLibrary library, String newName) throws InvalidCosplayNameException{
		if(!checkValidString(newName.trim())){
			throw new InvalidCosplayNameException(); 
		}
		
		library.renameCosplay(characterName, newName.trim());
		characterName = newName.trim();
		
	}
	
	/**
	 * Returns the numerical (integer) representation of this cosplay's category.
	 * 
	 * @return
	 * 			1 if the cosplay is current, 2 if it's finished, 3 if dropped.
	 */
	public int getCategory(){
		return type;
	}
	
	/**
	 * Sets or changes the cosplay's category.
	 * 
	 * @requires 
	 * 				the provided type is either current, finished, or dropped, written precisely.
	 * @throws 
	 * 				IllegalArgumentException if the type is not one of the above.
	 * @param type
	 * 				the category this cosplay belongs to.
	 */
	public void setCategory(String type){
		String category = type.toLowerCase().trim();
		
		if(category.equals("current")){
			this.type = 1;
		}
		
		else if(category.equals("finished")){
			this.type = 2;
		}
		else if(category.equals("dropped")){
			this.type = 3;
		}
		
		else{
			throw new IllegalArgumentException("Incorrect format.");
		}
	}
	
	/**
	 * Changes the cosplay to its string form for saving.
	 */
	public String toString(){
		String stringForm = "";
		
		stringForm = stringForm + openTag + "\n";
		stringForm = stringForm + openNameTag + characterName + closeNameTag + "\n";
		stringForm = stringForm + openTypeTag + type + closeTypeTag + "\n";
		stringForm = stringForm + fieldsToString();
		stringForm = stringForm + closeTag + "\n"; 
		
		return stringForm;
	}

	/*
	 * Method	:	fieldsToString
	 * Purpose	:	turns all the fields of this cosplay into their string form.
	 */
	private String fieldsToString(){
		String stringForm = "";
		
		for(String field: costInformation.keySet()){ //Also use a specific form for each field.
			stringForm = stringForm + openFieldTag + "\n";
			stringForm = stringForm + openFieldNTag + field + closeFieldNTag + "\n";
			stringForm = stringForm + openAmountTag + costInformation.get(field) + closeAmountTag + "\n";
			stringForm = stringForm + closeFieldTag + "\n";
		}
		
		return stringForm;
	}

}
