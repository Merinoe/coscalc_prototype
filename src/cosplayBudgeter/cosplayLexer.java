package cosplayBudgeter;
// Generated from cosplay.g4 by ANTLR 4.4
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class cosplayLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		START_COSPLAY=1, END_COSPLAY=2, START_FIELD=3, END_FIELD=4, START_NAME=5, 
		END_NAME=6, START_FIELDNAME=7, END_FIELDNAME=8, START_FIELDVAL=9, END_FIELDVAL=10, 
		START_TYPE=11, END_TYPE=12, TEXT=13, WS=14;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'"
	};
	public static final String[] ruleNames = {
		"START_COSPLAY", "END_COSPLAY", "START_FIELD", "END_FIELD", "START_NAME", 
		"END_NAME", "START_FIELDNAME", "END_FIELDNAME", "START_FIELDVAL", "END_FIELDVAL", 
		"START_TYPE", "END_TYPE", "TEXT", "WS"
	};


	public cosplayLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "cosplay.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\20\u0093\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\6\16\u0089\n\16"+
		"\r\16\16\16\u008a\3\17\6\17\u008e\n\17\r\17\16\17\u008f\3\17\3\17\2\2"+
		"\20\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\3\2\4\5\2\13\f>>@@\3\2\13\f\u0094\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3"+
		"\2\2\2\3\37\3\2\2\2\5)\3\2\2\2\7\64\3\2\2\2\t<\3\2\2\2\13E\3\2\2\2\rL"+
		"\3\2\2\2\17T\3\2\2\2\21\\\3\2\2\2\23e\3\2\2\2\25n\3\2\2\2\27x\3\2\2\2"+
		"\31\177\3\2\2\2\33\u0088\3\2\2\2\35\u008d\3\2\2\2\37 \7>\2\2 !\7e\2\2"+
		"!\"\7q\2\2\"#\7u\2\2#$\7r\2\2$%\7n\2\2%&\7c\2\2&\'\7{\2\2\'(\7@\2\2(\4"+
		"\3\2\2\2)*\7>\2\2*+\7\61\2\2+,\7e\2\2,-\7q\2\2-.\7u\2\2./\7r\2\2/\60\7"+
		"n\2\2\60\61\7c\2\2\61\62\7{\2\2\62\63\7@\2\2\63\6\3\2\2\2\64\65\7>\2\2"+
		"\65\66\7h\2\2\66\67\7k\2\2\678\7g\2\289\7n\2\29:\7f\2\2:;\7@\2\2;\b\3"+
		"\2\2\2<=\7>\2\2=>\7\61\2\2>?\7h\2\2?@\7k\2\2@A\7g\2\2AB\7n\2\2BC\7f\2"+
		"\2CD\7@\2\2D\n\3\2\2\2EF\7>\2\2FG\7p\2\2GH\7c\2\2HI\7o\2\2IJ\7g\2\2JK"+
		"\7@\2\2K\f\3\2\2\2LM\7>\2\2MN\7\61\2\2NO\7p\2\2OP\7c\2\2PQ\7o\2\2QR\7"+
		"g\2\2RS\7@\2\2S\16\3\2\2\2TU\7>\2\2UV\7h\2\2VW\7p\2\2WX\7c\2\2XY\7o\2"+
		"\2YZ\7g\2\2Z[\7@\2\2[\20\3\2\2\2\\]\7>\2\2]^\7\61\2\2^_\7h\2\2_`\7p\2"+
		"\2`a\7c\2\2ab\7o\2\2bc\7g\2\2cd\7@\2\2d\22\3\2\2\2ef\7>\2\2fg\7c\2\2g"+
		"h\7o\2\2hi\7q\2\2ij\7w\2\2jk\7p\2\2kl\7v\2\2lm\7@\2\2m\24\3\2\2\2no\7"+
		">\2\2op\7\61\2\2pq\7c\2\2qr\7o\2\2rs\7q\2\2st\7w\2\2tu\7p\2\2uv\7v\2\2"+
		"vw\7@\2\2w\26\3\2\2\2xy\7>\2\2yz\7v\2\2z{\7{\2\2{|\7r\2\2|}\7g\2\2}~\7"+
		"@\2\2~\30\3\2\2\2\177\u0080\7>\2\2\u0080\u0081\7\61\2\2\u0081\u0082\7"+
		"v\2\2\u0082\u0083\7{\2\2\u0083\u0084\7r\2\2\u0084\u0085\7g\2\2\u0085\u0086"+
		"\7@\2\2\u0086\32\3\2\2\2\u0087\u0089\n\2\2\2\u0088\u0087\3\2\2\2\u0089"+
		"\u008a\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\34\3\2\2"+
		"\2\u008c\u008e\t\3\2\2\u008d\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u008d"+
		"\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0092\b\17\2\2"+
		"\u0092\36\3\2\2\2\5\2\u008a\u008f\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}