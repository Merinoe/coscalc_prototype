package cosplayBudgeter;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


/**
 * Text-based user interface for CosCalc.
 * 
 * @author Merinoe
 *
 */
public class Project {
	private static CosplayLibrary library;
	private static Scanner inputReader; //Reader continuously accept input.
	
	public static void main(String args[]){
			library = new CosplayLibrary();
	
			System.out.println("Welcome to CosCalc! Please select from the options below.");
			System.out.println();
			projectMenu();
	}
	
	/*
	 * Method	:	projectMenu
	 * Purpose	:	displays a menu for users to select project manipulation options from.
	 */
	private static void projectMenu(){
		System.out.println();
		System.out.println("(C)reate a new project \t (A)lter a project \t (M)ove a project \t (D)elete a project \t (V)iew all projects \t (T)otal amount spent \t (S)ave data \t (L)oad data \t (E)xit");
		
		inputReader = new Scanner(System.in);
		String option = inputReader.next().toLowerCase();
		
		if(option.equals("c")){
			cosplayMenu(createCosplay());
		}
		else if(option.equals("a")){
			changeCosplay();
		}
		else if(option.equals("m")){
			moveCosplay();
		}
		else if(option.equals("d")){
			deleteCosplay();
		}
		else if(option.equals("v")){
			viewCosplays();
		}
		else if(option.equals("t")){
			System.out.println("You have spent $" + library.total() + " in total.");
		}
		else if(option.equals("s")){
			PrintWriter saver = null;
			
			try {
				saver = new PrintWriter("cosdata.txt");
				saver.println(library.toStringForm());
				saver.close();
				System.out.println("Data saved!");
			} catch (FileNotFoundException e) {
				System.out.println("Save unsuccessful.");
			}
		}
		else if(option.equals("l")){
			try{
			library = CosplayLibrary.restoreLibrary("cosdata.txt");
			System.out.println("File loaded.");}
			catch(FileNotFoundException e){
				System.out.println("Error: Data file missing.");
			}
		}
		else if(option.equals("e")){
			return;
		}
		
		projectMenu(); //Recursively present options.
		
	}
	
	/*
	 * Method	:	moveCosplay
	 * Purpose	:	allows a user to change a cosplay's category.
	 */
	private static void moveCosplay() {
		System.out.println("Please enter the name of the cosplay you would like to move: ");
		String cosName = retrieveInputText();
		
		System.out.println("Where would you like to move " + cosName + "?");
		int category = chooseCategory();
		
		if(category== 0){ //0 indicates no change.
			return;
		}
		
		try {
			library.move(cosName, category);
		} catch (InvalidCosplayException e) {
			System.out.println("Cosplay does not exist! Please try again.");
			moveCosplay();
		}
		
		
	}
	
	/*
	 * Method	:	chooseCategory
	 * Purpose	:	prompts the user to select one of three possible cosplay categories.
	 */
	private static int chooseCategory(){
		System.out.println("(C)urrent Cosplays \t (F)inished Cosplays \t (D)ropped Cosplays \t(N)o change");
		
		inputReader = new Scanner(System.in);
		String option = inputReader.next().toLowerCase().trim();
		
		if(option.equals("c")){
			return 1;
		}
		
		else if(option.equals("f")){
			return 2;
		}
		
		else if(option.equals("d")){
			return 3;
		}
		
		else if(option.equals("n")){
			return 0;
		}
		else{
			return chooseCategory(); //Recursively call until an option is chosen.
		}
	}

	
	/*
	 * Method	:	viewCosplays
	 * Purpose	:	prints a full listing of all of the library's cosplays in their 
	 * 				respective catogories, in alphabetical order.
	 */
	private static void viewCosplays() {
		library.printLibrary();
		
	}

	/*
	 * Method	:	deleteCosplay()
	 * Purpose	:	allows a user to delete a cosplay of their choosing.
	 */
	private static void deleteCosplay() {
		System.out.println("Please enter the name of the cosplay you would like to delete: ");
		String cosName = retrieveInputText();
		
		try {
			library.deleteCosplay(cosName);
			System.out.println("Cosplay deleted.");
		} catch (InvalidCosplayException e) {
			System.out.println("Invalid name! Please try again.");
			deleteCosplay();
		}
		
	}

	/*
	 * Method	:	changeCosplay
	 * Purpose	:	allows a user to access and modify a specific cosplay.
	 */
	private static void changeCosplay() {
		System.out.println("Please enter the name of the cosplay you would like to modify: ");
		String cosName = retrieveInputText();
		
		cosplayMenu(cosName);
		
	}
	
	/*
	 * Method	:	retrieveInputText
	 * Returns	:	the line of text inputed by the user.
	 * Purpose	:	retrieves the full line of text that the user has	
	 * 				inputed to the console.
	 */
	private static String retrieveInputText(){
		String cosName = "";
		
		while(inputReader.hasNext()){
			inputReader.nextLine(); //Ignore the blank line.
			cosName =  inputReader.nextLine();
			break; //Exit the loop once retrieved.
		}
		
		return cosName;
	}

	/*
	 * Method	:	createCosplay
	 * Returns	:	the cosplay if it was successfully created.
	 * Purpose	:	allows the user to create a cosplay.
	 */
	private static Cosplay createCosplay(){
		System.out.println("Please enter the name of your Cosplay: ");
		String cosName = retrieveInputText();
		
		System.out.println("Please enter its category (current cosplay is the default category if none is chosen): ");
		int category = chooseCategory();
		
		try {
			return library.addCosplay(cosName, category); //Return the created cosplay if successful.
			
		} catch (InvalidCosplayNameException e) {
			System.out.println(cosName + " is not a valid name for a cosplay. Please try again.");
			return createCosplay(); //Recursively ask for cosplay name if unsuccessful.
		}
	
	
	}

	/*
	 * Method	:	cosplayMenu
	 * Param(s)	:	cosName	- the name of the cosplay to manipulate
	 * Purpose	:	to display options for manipulating a specific cosplay
	 */
	private static void cosplayMenu(String cosName){
		Cosplay currCosplay = library.findCosplay(cosName);
		cosplayMenu(currCosplay);
	}
	
	/*
	 * Method	:	cosplayMenu
	 * Param(s)	:	currentCosplay	- the cosplay to manipulate
	 * Purpose	: 	to display options for manipulating a specific cosplay
	 */
	private static void cosplayMenu(Cosplay currentCosplay){
		System.out.println();
		System.out.println("(C)reate a new field \t (E)dit a field  \t (A)dd to a field \t (D)elete this cosplay \t (R)ename this cosplay \t (P)rint a cost summary \t (S)crap a field \t (T)o main menu");
	
		inputReader = new Scanner(System.in);
		String option = inputReader.next().toLowerCase().trim();	
		
		if(option.equals("c")){
			createField(currentCosplay);
		}
		else if(option.equals("e")){
			editField(currentCosplay);
		}
		else if(option.equals("a")){
			addToField(currentCosplay);
		}
		else if(option.equals("r")){
			renameCos(currentCosplay);
		}
		else if (option.equals("p")){
			currentCosplay.printSummary();
		}
		else if(option.equals("s")){
			deleteField(currentCosplay);
		}
		else if (option.equals("d")){
			try {
				library.deleteCosplay(currentCosplay.retrieveName());
				System.out.println("Cosplay deleted.");
				return;
				} catch (InvalidCosplayException e) {
				// Catch would be unexpected, since the cosplay is confirmed to exist.
			}
		}
		else if (option.equals("t")){
			return;
		}
		
		cosplayMenu(currentCosplay); //Display recursively until an option is chosen.
	}

	/*
	 * Method	:	deleteField
	 * Param(s)	:	currentCosplay	- the cosplay containing the field to delete
	 * Purpose	:	allows a user to delete a field
	 */
	private static void deleteField(Cosplay currentCosplay) {
		System.out.println("Which field would you like to delete?");
		String field = retrieveInputText();
		
		try {
			currentCosplay.deleteField(field);
			System.out.println("Delete successful!");
		} catch (InvalidFieldException e) {
			System.out.println("Field does not exist! Delete unsuccessful.");
		}
		
	}

	/*
	 * Method	:	renameCos
	 * Param(s)	:	currentCosplay	- the cosplay to rename
	 * Purpose	:	allows a user to rename a cosplay
	 */
	private static void renameCos(Cosplay currentCosplay) {
		System.out.println("Which would you like to rename your cosplay as?");
		String newName = retrieveInputText();
		
		try {
			currentCosplay.renameCosplay(library, newName);
		} catch (InvalidCosplayNameException e) {
			System.out.println(newName + " is not a valid name! Please try again.");
			renameCos(currentCosplay);
		}
		
	}

	/*
	 * Method	:	addToField
	 * Param(s)	:	currentCosplay	- the cosplay containing the field to modify
	 * Purpose	:	allows a user to add a dollar value to a field
	 */
	private static void addToField(Cosplay currentCosplay) {
		System.out.println("Which field would you like to add money to?");
		String field = retrieveInputText();
		
		System.out.println();
		System.out.println("What amount would you like to add to this field?");
		
		double cost = inputReader.nextDouble();
		
		try {
			currentCosplay.addToExistingValue(field, cost);
		} catch (InvalidFieldException e) {
			System.out.println("Invalid field! Please try again.");//Recursively ask for a correct input.
			addToField(currentCosplay);
		}
		
	}

	/*
	 * Method	:	editField
	 * Param(s)	:	currentCosplay	- the cosplay containing the field to edit
	 * Purpose	:	allows a user to edit a field
	 */
	private static void editField(Cosplay currentCosplay) {
		System.out.println("Which field would you like to edit?");
		String field = retrieveInputText();
		
		System.out.println("(E)dit the field amount \t (R)ename this field \t (C)ancel");
		String option = inputReader.next().trim().toLowerCase();
		
		if(option.equals("e")){
			editFieldAmount(currentCosplay, field);
			return;
		}
		else if(option.equals("r")){
			editFieldName(currentCosplay, field);
			return;
		}
		else if(option.equals("c")){
			return;
		}
		
		editField(currentCosplay);  //recursively present options.
		
	}

	/*
	 * Method	:	editFieldName
	 * Param(s)	:	currentCosplay	- the cosplay containing the field to rename
	 * 				field			- the field to rename
	 * Purpose	:	allows a user to rename a field
	 */
	private static void editFieldName(Cosplay currentCosplay, String field){
		System.out.println();
		System.out.println("What is the new name of this field?");
		
		String newField = retrieveInputText();
		
		try {
			currentCosplay.renameField(field, newField);
		} catch (InvalidFieldException e) {
			System.out.println("Invalid field name! Please try again.");//Recursively ask for a correct input.
			editField(currentCosplay);
		}
	}

	/*
	 * Method	:	editFieldAmount
	 * Param(s)	:	currentCosplay	- the cosplay containing the field to edit
	 * 				field			- the field to modify
	 * Purpose	:	allows a user to change the value of a field
	 */
	private static void editFieldAmount(Cosplay currentCosplay, String field){
		System.out.println();
		System.out.println("What amount would you like to place in this field?");
		
		double cost = inputReader.nextDouble();
		
		try {
			currentCosplay.editInformationField(field, cost);
		} catch (InvalidFieldException e) {
			System.out.println("Invalid field! Please try again.");//Recursively ask for a correct input.
			editField(currentCosplay);
		}
	}

	/*
	 * Method	:	createField
	 * Param(s)	:	currentCosplay	- the cosplay to add a field to
	 * Purpose	:	allows a user to create a field in a cosplay.
	 */
	private static void createField(Cosplay currentCosplay) {
		System.out.println("What is the name of the field you would like to create?");
		String field = retrieveInputText();
		
		System.out.println();
		System.out.println("What amount would you like to place in this field?");
		
		double cost = inputReader.nextDouble();
		
		try {
			currentCosplay.addInformationField(field, cost);
		} catch (InvalidFieldException e) {
			System.out.println("Invalid field! Please try again.");//Recursively ask for a correct input.
			createField(currentCosplay);
		}
		
	}

	
	
}


