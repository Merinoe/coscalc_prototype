package cosplayBudgeter;
// Generated from cosplay.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link cosplayParser}.
 */
public interface cosplayListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link cosplayParser#amount}.
	 * @param ctx the parse tree
	 */
	void enterAmount(@NotNull cosplayParser.AmountContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#amount}.
	 * @param ctx the parse tree
	 */
	void exitAmount(@NotNull cosplayParser.AmountContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#field}.
	 * @param ctx the parse tree
	 */
	void enterField(@NotNull cosplayParser.FieldContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#field}.
	 * @param ctx the parse tree
	 */
	void exitField(@NotNull cosplayParser.FieldContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#cosplay}.
	 * @param ctx the parse tree
	 */
	void enterCosplay(@NotNull cosplayParser.CosplayContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#cosplay}.
	 * @param ctx the parse tree
	 */
	void exitCosplay(@NotNull cosplayParser.CosplayContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#root}.
	 * @param ctx the parse tree
	 */
	void enterRoot(@NotNull cosplayParser.RootContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#root}.
	 * @param ctx the parse tree
	 */
	void exitRoot(@NotNull cosplayParser.RootContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(@NotNull cosplayParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(@NotNull cosplayParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#library}.
	 * @param ctx the parse tree
	 */
	void enterLibrary(@NotNull cosplayParser.LibraryContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#library}.
	 * @param ctx the parse tree
	 */
	void exitLibrary(@NotNull cosplayParser.LibraryContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(@NotNull cosplayParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(@NotNull cosplayParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link cosplayParser#fieldname}.
	 * @param ctx the parse tree
	 */
	void enterFieldname(@NotNull cosplayParser.FieldnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link cosplayParser#fieldname}.
	 * @param ctx the parse tree
	 */
	void exitFieldname(@NotNull cosplayParser.FieldnameContext ctx);
}