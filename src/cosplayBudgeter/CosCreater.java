package cosplayBudgeter;

import java.util.HashMap;

import org.antlr.v4.runtime.tree.TerminalNode;

//Class and corresponding grammars in this project were
//written based on a CatalogueListener created by Sathish Gopalakrishnan.
public class CosCreater extends cosplayBaseListener {
	//Create a temporary library to return.
	private CosplayLibrary library = new CosplayLibrary();
	
	//Temporary Cosplay variables
	private String currName;
	private int currType;
	private Cosplay currCosplay;
	
	//Temporary field variables.
	private HashMap<String, Double> currCostInfo;
	private String currFieldName;
	private double currFieldAmount;
	
	@Override
	public void enterCosplay(cosplayParser.CosplayContext ctx){
		currCostInfo = new HashMap<String, Double>();
	}
	
	@Override
	public void enterName(cosplayParser.NameContext ctx){
		TerminalNode token = ctx.TEXT();
		currName = token.getText(); //Save the name retrieved.
	}
	
	@Override 
	public void enterType(cosplayParser.TypeContext ctx){
		TerminalNode token = ctx.TEXT();
		currType = Integer.parseInt(token.getText()); //Save the type number retrieved.
	}
	
	@Override
	public void enterFieldname(cosplayParser.FieldnameContext ctx){ 
		TerminalNode token = ctx.TEXT();
		currFieldName = token.getText(); //Save the name of the field.
	}
	
	@Override
	public void enterAmount(cosplayParser.AmountContext ctx){
		TerminalNode token = ctx.TEXT();
		currFieldAmount = Double.parseDouble(token.getText()); //Save the field's value.
	}

	@Override
	public void exitField(cosplayParser.FieldContext ctx){
		currCostInfo.put(currFieldName, currFieldAmount); //Save each field into a temporary map after all information for the field has been collected.
	}
	
	@Override
	public void exitCosplay(cosplayParser.CosplayContext ctx){
		try {
			currCosplay = new Cosplay(currName);
			
			for(String field: currCostInfo.keySet()){
				currCosplay.addInformationField(field, currCostInfo.get(field));
			}
			
			library.addCosplay(currCosplay, currType);
			
		} catch (InvalidCosplayNameException | InvalidFieldException e) {
			System.out.println("Error: corrupted data file."); //An accepted data file should not cause unexpected exceptions to be thrown.
		}
	
		
	}
	
	/**
	 * Returns a CosplayLibrary object after a data file has been fully
	 * parsed.
	 * 
	 * @requires
	 * 		the parseTree method must have already been walked with this 
	 * 		CosCreater object.
	 * @return
	 * 		the CosplayLibrary represented by a valid data file.
	 */
	public CosplayLibrary getLibrary(){
		return library;
	}
}

