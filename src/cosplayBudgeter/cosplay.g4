grammar cosplay;

// Lexer Rules

START_COSPLAY: 	'<cosplay>' ;
END_COSPLAY:	'</cosplay>' ;
START_FIELD:	'<field>' ;
END_FIELD:		'</field>' ;
START_NAME:		'<name>' ;
END_NAME:		'</name>' ;
START_FIELDNAME: '<fname>';
END_FIELDNAME: '</fname>';
START_FIELDVAL:		'<amount>' ;
END_FIELDVAL:		'</amount>' ;
START_TYPE:		'<type>';
END_TYPE:		'</type>';
TEXT:			~[\t\n<>]+ ;
WS:				[\t\n]+ -> skip ;

// Parser Rules

root: 		library EOF ;
name:		START_NAME TEXT END_NAME ;
fieldname:  START_FIELDNAME TEXT END_FIELDNAME ;
type:		START_TYPE TEXT END_TYPE ;
library: 	( cosplay )* ;
cosplay:	START_COSPLAY (name) (type) ( field)* END_COSPLAY ;
field:		START_FIELD (fieldname) (amount)+ END_FIELD ;
amount:	    START_FIELDVAL TEXT END_FIELDVAL ;