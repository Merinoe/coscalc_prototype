package cosplayBudgeter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.TreeMap;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import cosplayBudgeter.InvalidCosplayException;
import cosplayBudgeter.InvalidCosplayNameException;

/**
 * A basic storage or library for all cosplay projects added by the user.
 * 
 * @author	Merinoe 
 *
 */

public class CosplayLibrary {
	/*Rep Invariant: currentCosplays, droppedCosplays, and finishedCosplays are not null
	 *				 and represent cosplays planned or done by the user of this library.
	 *				 A directory of cosplays objects corresponding to their character names is 
	 *				 stored in this manner.
	 *
	 *Note: 		 Cosplays cannot share the same name. If the same character is being cosplayed, 
	 *				 the version should be specified. For example, Rin Tohsaka (casual) and Rin Tohsaka 
	 *				 (Kaleido Ruby) cannot both be named Rin Tohsaka. Costumes should be noted in the 
	 *				 character name in the case of multiple versions.
	 */
	
	//Cosplay categories
	private TreeMap<String, Cosplay> currentCosplays;
	private TreeMap<String, Cosplay> droppedCosplays;
	private TreeMap<String, Cosplay> finishedCosplays;

	/**
	 * This class creates a storage for a collection of cosplay projects. 
	 * Cosplays can be stored in three possible categories: current cosplays,
	 * finished cosplays, and dropped cosplays, often represented by the.
	 * integers 1, 2, and 3, respectively. These categories can later
	 * be changed, while cosplays can be found, accessed, and modified.
	 * 
	 */
	public CosplayLibrary(){
		//Initiate empty storages for all categories.
		currentCosplays = new TreeMap<String, Cosplay>();
		droppedCosplays = new TreeMap<String, Cosplay>();
		finishedCosplays = new TreeMap<String, Cosplay>();
	}
	
	/**
	 * Adds an existing Cosplay object to the library.
	 * 
	 * @param currCos	
	 * 					the cosplay to add into this CosplayLibrary object.
	 * @param category	
	 * 					the integer representation of the cosplay's intended
	 * 					category. This value is 1 for a current cosplay,
	 * 					2 for a finished cosplay, and 3 for a dropped cosplay.
	 * 					If the number provided is not one of the three valid
	 * 					category representations, the cosplay will be added
	 * 					as a current cosplay by default.
	 * 
	 */
	public void addCosplay(Cosplay currCos, int category){
		placeCosInCategory(currCos.retrieveName(), currCos, category);
	}
	
	/**
	 * Adds a new, non-previously existing Cosplay object to the library.
	 * 
	 * @param cosplayName					
	 * 										the name of the new cosplay to add.
	 * @param category						
	 * 										the integer representation of the cosplay's intended
	 * 										category. This value is 1 for a current cosplay,
	 * 										2 for a finished cosplay, and 3 for a dropped cosplay.
	 * 										If the number provided is not one of the three valid
	 * 										category representations, the cosplay will be added
	 * 										as a current cosplay by default.
	 * @throws InvalidCosplayNameException	
	 * 										if the cosplay of the same name already exists in this library,
	 * 										is an empty string, or null.
	 * @return							
	 * 										the Cosplay object that was successfully added.
	 */
	public Cosplay addCosplay(String cosplayName, int category) throws InvalidCosplayNameException{
		//Confirm that the cosplay is not already part of the library.
		if(droppedCosplays.containsKey(cosplayName) || currentCosplays.containsKey(cosplayName) || finishedCosplays.containsKey(cosplayName)){
			throw new InvalidCosplayNameException(); 
		}
		
		Cosplay currCosplay = new Cosplay(cosplayName); //If the name provided is blank or null, the Cosplay class will throw the same exception.
		
		placeCosInCategory(cosplayName, currCosplay, category);
		return currCosplay;
	}
	
	/*
	 * Method	: 	placeCosInCetegory
	 * Param(s)	:	cosplayName - the name of the cosplay to place into a given category.
	 * 				currCosplay - the cosplay object to place.
	 * 				category 	- the numerical representation of the category to place the given cosplay.
	 * 							  1 represents current, 2 finished, and 3 dropped. If an invalid integer
	 * 							  is provided, the cosplay will be considered as a current cosplay by
	 * 							  default.
	 * Purpose	:	Places a cosplay in a specified category.
	 */
	private void placeCosInCategory(String cosplayName, Cosplay currCosplay, int category){
		if(category == 2){
			currCosplay.setCategory("finished");
			finishedCosplays.put(cosplayName, currCosplay);
		}
		else if(category == 3){
			currCosplay.setCategory("dropped");
			droppedCosplays.put(cosplayName, currCosplay);
		}
		
		else{
			currCosplay.setCategory("current");
			currentCosplays.put(cosplayName, currCosplay);
		}
	}
	
	/**
	 * Permanently deletes the given from the library.
	 * Warning: the user will not be prompted for a confirmation 
	 * before the specified cosplay is deleted.
	 * 
	 * @param cosplayName
	 * 									the exact name of the cosplay to delete. 
	 * @throws InvalidCosplayException 
	 * 									if the cosplay does not exist in this CosplayLibrary.
	 */
	public void deleteCosplay(String cosplayName) throws InvalidCosplayException{		
		if(droppedCosplays.containsKey(cosplayName)){
			droppedCosplays.remove(cosplayName);
		}
		
		else if(currentCosplays.containsKey(cosplayName)){
			currentCosplays.remove(cosplayName);
		}
		else if(finishedCosplays.containsKey(cosplayName)){
			finishedCosplays.remove(cosplayName);
		}
		else{
			throw new InvalidCosplayException();  //If the cosplay cannot be found in either of the three categories, then it does not exist.
		}
		
	}
	
	/**
	 * Searches for a cosplay, using the provided query.
	 * 
	 * @param query
	 * 					the name of the cosplay to search for.
	 * @return
	 * 					the cosplay object if an exact match is found. If partial
	 * 					matches are found, suggested matches are displayed, but 
	 * 					null is returned.
	 */
	public Cosplay findCosplay(String query){
		if(currentCosplays.containsKey(query)){
			return currentCosplays.get(query);
		}
		
		else if(finishedCosplays.containsKey(query)){
			return finishedCosplays.get(query);
		}
		else if(droppedCosplays.containsKey(query)){
			return droppedCosplays.get(query);
		}
		
		System.out.println("Do you mean: ");
		//Use booleans to confirm if any matches are found.
		boolean currCheck = printPartialMatch(currentCosplays, query);
		boolean finCheck = printPartialMatch(finishedCosplays, query);
		boolean dropCheck = printPartialMatch(droppedCosplays, query);
		
		//Only print this if nothing can be found.
		if(!(finCheck || currCheck || dropCheck)){
			System.out.println("Error: no matches found.");
		}
		
		return null;
		
	}
	
	/*
	 * Method	:	printPartialMatch
	 * Param(s)	:	dataSet - the treemap to search through
	 * 				query	- a string representing the partial match to search for.
	 * Purpose	:	to determine and print out partial cosplay name match in a provided
	 * 				dataset, based on a query.
	 * Returns	:	true if at least one partial match was found, false otherwise.
	 */
	private boolean printPartialMatch(TreeMap<String, Cosplay> dataSet, String query){
		boolean found = false; //Check if any matches were found.
		
		for(String cosplayNames: dataSet.keySet()){
			//Check each title for a match.
			if(cosplayNames.contains(query)){
				found = true;
				System.out.println(cosplayNames);
			}
		}
		
		return found;
	}
	

	/**
	 * Changes a cosplay's category, which may be current, finished, or dropped.
	 * 
	 * @param cosName
	 * 										the exact name of the cosplay to move.
	 * @param option
	 * 										the integer representation of the cosplay's new
	 * 										category. This value is 1 for a current cosplay,
	 * 										2 for a finished cosplay, and 3 for a dropped cosplay.
	 * 										
	 * @throws InvalidCosplayException 
	 * 										if the provided cosplay name does not correspond to any
	 * 										cosplay stored in this CosplayLibrary.
	 * @throws IllegalArgumentException
	 * 										if a number other than 1-3, inclusive, is provided.
	 * 			
	 */
	public void move(String cosName, int option) throws InvalidCosplayException {
		//Confirm that the option value is valid.
		if(option < 1 || option > 3){
			throw new IllegalArgumentException("Option must be a number between 1 and 3, inclusive.");
		}
		
		//Locate the cosplay.
		Cosplay currCos = findCosplay(cosName);
		deleteCosplay(cosName);	
		
		//Evaluate the provided option.
		if(option == 1){
			currentCosplays.put(cosName, currCos);
			currCos.setCategory("current");
		}
		
		else if (option == 2){
			finishedCosplays.put(cosName, currCos);
			currCos.setCategory("finished");
		}
		else if (option == 3){
			droppedCosplays.put(cosName, currCos);
			currCos.setCategory("dropped");
		}
		
	}
	

	/**
	 * Prints out all the cosplays in this library, in their respective categories,
	 * in alphabetical order. If no cosplays exist in a category, the space below the
	 * category will simply be blank.
	 */
	public void printLibrary() {
		System.out.println();
		System.out.println("Current Cosplays:");
		printCos(currentCosplays);
		
		System.out.println();
		System.out.println("Finished Cosplays:");
		printCos(finishedCosplays);
		
		System.out.println();
		System.out.println("Dropped Cosplays:");
		printCos(droppedCosplays);
		
		
	}
	
	/**
	 * Changes the cosplay "oldname" to "newname".
	 * 
	 * @requires
	 * 										the cosplay corresponding to oldname's category has
	 * 										already been changed, and oldname represents an existing
	 * 										cosplay in this CosplayLibrary.
	 * @param oldName
	 * 										the exact name of the cosplay to rename.
	 * @param newName	
	 * 										the new name of the cosplay.
	 * @throws InvalidCosplayNameException 	
	 * 										if the provided newName already exists.
	 */
	protected void renameCosplay(String oldName, String newName) throws InvalidCosplayNameException{
		if(droppedCosplays.containsKey(newName) || currentCosplays.containsKey(newName) || finishedCosplays.containsKey(newName)){
			throw new InvalidCosplayNameException();
		}
		
		//Replacement is only done if the cosplay exists. A duplicate cosplay cannot exist in multiple categories.
		checkAndRenameCos(currentCosplays, oldName, newName);
		checkAndRenameCos(finishedCosplays, oldName, newName);
		checkAndRenameCos(droppedCosplays, oldName, newName);
		
	}	

	/*
	 * Method	:	checkAndRenameCos
	 * Param(s)	:	dataSet - the treemap to search through.
	 * 				oldName	- the exact name of the cosplay to rename.
	 * 				newName - the new name of the cosplay
	 * Purpose	: 	to check if a cosplay exists in a provided category. If it does, the cosplay is
	 * 				renamed from oldName to newName.
	 */
	private void checkAndRenameCos(TreeMap<String, Cosplay> dataSet, String oldName, String newName){
		if(dataSet.containsKey(oldName)){ 
			Cosplay currCos = dataSet.get(oldName);
			dataSet.remove(oldName);
			dataSet.put(newName, currCos);
		}
	}
	
	/*
	 * Method	:	printCos
	 * Param(s)	:	dataSet - the treemap to search through.
	 * Purpose	:	to print out the name of all the cosplays saved in the
	 * 				specified dataset.
	 */
	private void printCos(TreeMap<String, Cosplay> dataSet){
		for(String cosName: dataSet.keySet()){
			System.out.println(cosName);
		}
		
	}

	/**
	 * Determines the total cost of all cosplay projects in this library.
	 * 
	 * @return
	 * 			the sum of the price of all cosplays stored.
	 */
	public double total() {
		double amount = 0; //If no money is spent, the total amount spent is $0.
		
		//Add the total from each data set.
		amount += sumDataSet(currentCosplays);
		amount += sumDataSet(finishedCosplays);
		amount += sumDataSet(droppedCosplays);
		
		return amount;
		
	}
	

	/*
	 * Method	:	sumDataSet
	 * Param(s)	:	dataSet - the map to search through
	 * Purpose	:	to calculate the total cost of all the cosplays 
	 * 				the provided dataset.
	 * Returns	:	the total cost of all the cosplays in this dataSet, 
	 * 				rounded to two decimal places.
	 */
	private double sumDataSet(TreeMap<String, Cosplay> dataSet){
		double amount = 0;
		
		for(Cosplay currCos: dataSet.values()){
			amount += currCos.retrieveTotal(); //All values retrieved are to two decimal places, so no additional rounding is needed.
		}
		
		return amount;
	}
	
	/**
	 * Converts the CosplayLibrary into its string representation. 
	 * This string representation can be used with the <i>restoreLibrary</i>
	 * method to recreate a CosplayLibrary object.
	 * 
	 * @return
	 * 			the string representation of this CosplayLibrary object.
	 */
	public String toStringForm(){
		String stringRep = "";
		
		//Add the string form of all three cosplay categories.
		stringRep = stringRep + addStringByDataset(currentCosplays);
		stringRep = stringRep + addStringByDataset(finishedCosplays);
		stringRep = stringRep + addStringByDataset(droppedCosplays);
		
		return stringRep;
	}
	
	/*
	 * Method	:	addStringByDataset
	 * Param(s)	:	dataSet - the map to search through
	 * Purpose	:	to generate the string form of the provided 
	 * 				dataset.
	 * Returns	:	a string that concatenates the string form
	 * 				of all the Cosplays in the provided dataset.
	 */
	private String addStringByDataset(TreeMap<String, Cosplay> dataSet){
		String stringRep = "";
		
		//Adds the string form of all the cosplays within the dataset.
		for(Cosplay currCos: dataSet.values()){
			stringRep = stringRep + currCos.toString();
		}
		
		return stringRep;
	}
	
	/**
	 * Restores a CosplayLibrary object using a given data file.
	 * <p>Note: the data file must use the same grammar outputted by
	 * the <i>toStringForm</i> method.</p>
	 * 
	 * @param fileName
	 * 									the name of the data file to be read, including extension.
	 * @throws FileNotFoundException
	 * 									if the specified file cannot be located.
	 * @return
	 * 									the CosplayLibrary constructed using the data file.
	 * 
	 */
	public static CosplayLibrary restoreLibrary(String fileName) throws FileNotFoundException{
		String stringRep = obtainStringRepFromFile(fileName);
		
		CharStream stream = new ANTLRInputStream(stringRep);
		cosplayLexer lexer = new cosplayLexer(stream);
		TokenStream tokens = new CommonTokenStream(lexer);

		cosplayParser parser = new cosplayParser(tokens);
		ParseTree tree = parser.root();

		ParseTreeWalker walker = new ParseTreeWalker();
		CosCreater listener = new CosCreater();
		walker.walk(listener, tree);
		CosplayLibrary cosLib = listener.getLibrary();
		
		
		return cosLib;
	}
	
	
	/*
	 * Method	:	obtainStringRepFromFile
	 * Param(s)	:	filename - the name of the file to read
	 * Throws	:	FileNotFoundException - if the file cannot be located.
	 * Returns	:	A string containing all the contents of the file.
	 * Purpose	: 	to transpose the contents of the data file to a string for parsing.
	 */
	private static String obtainStringRepFromFile(String filename) throws FileNotFoundException{
		Scanner fileReader = new Scanner(new File(filename));
		String stream = "";
		
		while(fileReader.hasNextLine()){
			stream = stream + fileReader.nextLine() + "\n";
		}
		
		fileReader.close();
		return stream;
	}


}
