package cosplayBudgeter;
// Generated from cosplay.g4 by ANTLR 4.4
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class cosplayParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		START_COSPLAY=1, END_COSPLAY=2, START_FIELD=3, END_FIELD=4, START_NAME=5, 
		END_NAME=6, START_FIELDNAME=7, END_FIELDNAME=8, START_FIELDVAL=9, END_FIELDVAL=10, 
		START_TYPE=11, END_TYPE=12, TEXT=13, WS=14;
	public static final String[] tokenNames = {
		"<INVALID>", "'<cosplay>'", "'</cosplay>'", "'<field>'", "'</field>'", 
		"'<name>'", "'</name>'", "'<fname>'", "'</fname>'", "'<amount>'", "'</amount>'", 
		"'<type>'", "'</type>'", "TEXT", "WS"
	};
	public static final int
		RULE_root = 0, RULE_name = 1, RULE_fieldname = 2, RULE_type = 3, RULE_library = 4, 
		RULE_cosplay = 5, RULE_field = 6, RULE_amount = 7;
	public static final String[] ruleNames = {
		"root", "name", "fieldname", "type", "library", "cosplay", "field", "amount"
	};

	@Override
	public String getGrammarFileName() { return "cosplay.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public cosplayParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RootContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(cosplayParser.EOF, 0); }
		public LibraryContext library() {
			return getRuleContext(LibraryContext.class,0);
		}
		public RootContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_root; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterRoot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitRoot(this);
		}
	}

	public final RootContext root() throws RecognitionException {
		RootContext _localctx = new RootContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_root);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16); library();
			setState(17); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode END_NAME() { return getToken(cosplayParser.END_NAME, 0); }
		public TerminalNode TEXT() { return getToken(cosplayParser.TEXT, 0); }
		public TerminalNode START_NAME() { return getToken(cosplayParser.START_NAME, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitName(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(19); match(START_NAME);
			setState(20); match(TEXT);
			setState(21); match(END_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldnameContext extends ParserRuleContext {
		public TerminalNode TEXT() { return getToken(cosplayParser.TEXT, 0); }
		public TerminalNode END_FIELDNAME() { return getToken(cosplayParser.END_FIELDNAME, 0); }
		public TerminalNode START_FIELDNAME() { return getToken(cosplayParser.START_FIELDNAME, 0); }
		public FieldnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldname; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterFieldname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitFieldname(this);
		}
	}

	public final FieldnameContext fieldname() throws RecognitionException {
		FieldnameContext _localctx = new FieldnameContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_fieldname);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23); match(START_FIELDNAME);
			setState(24); match(TEXT);
			setState(25); match(END_FIELDNAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode TEXT() { return getToken(cosplayParser.TEXT, 0); }
		public TerminalNode START_TYPE() { return getToken(cosplayParser.START_TYPE, 0); }
		public TerminalNode END_TYPE() { return getToken(cosplayParser.END_TYPE, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27); match(START_TYPE);
			setState(28); match(TEXT);
			setState(29); match(END_TYPE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LibraryContext extends ParserRuleContext {
		public List<CosplayContext> cosplay() {
			return getRuleContexts(CosplayContext.class);
		}
		public CosplayContext cosplay(int i) {
			return getRuleContext(CosplayContext.class,i);
		}
		public LibraryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_library; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterLibrary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitLibrary(this);
		}
	}

	public final LibraryContext library() throws RecognitionException {
		LibraryContext _localctx = new LibraryContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_library);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==START_COSPLAY) {
				{
				{
				setState(31); cosplay();
				}
				}
				setState(36);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CosplayContext extends ParserRuleContext {
		public TerminalNode START_COSPLAY() { return getToken(cosplayParser.START_COSPLAY, 0); }
		public FieldContext field(int i) {
			return getRuleContext(FieldContext.class,i);
		}
		public TerminalNode END_COSPLAY() { return getToken(cosplayParser.END_COSPLAY, 0); }
		public List<FieldContext> field() {
			return getRuleContexts(FieldContext.class);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public CosplayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cosplay; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterCosplay(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitCosplay(this);
		}
	}

	public final CosplayContext cosplay() throws RecognitionException {
		CosplayContext _localctx = new CosplayContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_cosplay);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37); match(START_COSPLAY);
			{
			setState(38); name();
			}
			{
			setState(39); type();
			}
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==START_FIELD) {
				{
				{
				setState(40); field();
				}
				}
				setState(45);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(46); match(END_COSPLAY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldContext extends ParserRuleContext {
		public AmountContext amount(int i) {
			return getRuleContext(AmountContext.class,i);
		}
		public List<AmountContext> amount() {
			return getRuleContexts(AmountContext.class);
		}
		public TerminalNode START_FIELD() { return getToken(cosplayParser.START_FIELD, 0); }
		public TerminalNode END_FIELD() { return getToken(cosplayParser.END_FIELD, 0); }
		public FieldnameContext fieldname() {
			return getRuleContext(FieldnameContext.class,0);
		}
		public FieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_field; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterField(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitField(this);
		}
	}

	public final FieldContext field() throws RecognitionException {
		FieldContext _localctx = new FieldContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_field);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); match(START_FIELD);
			{
			setState(49); fieldname();
			}
			setState(51); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(50); amount();
				}
				}
				setState(53); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==START_FIELDVAL );
			setState(55); match(END_FIELD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmountContext extends ParserRuleContext {
		public TerminalNode END_FIELDVAL() { return getToken(cosplayParser.END_FIELDVAL, 0); }
		public TerminalNode TEXT() { return getToken(cosplayParser.TEXT, 0); }
		public TerminalNode START_FIELDVAL() { return getToken(cosplayParser.START_FIELDVAL, 0); }
		public AmountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_amount; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).enterAmount(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cosplayListener ) ((cosplayListener)listener).exitAmount(this);
		}
	}

	public final AmountContext amount() throws RecognitionException {
		AmountContext _localctx = new AmountContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_amount);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57); match(START_FIELDVAL);
			setState(58); match(TEXT);
			setState(59); match(END_FIELDVAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\20@\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\3\3\3"+
		"\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\7\6#\n\6\f\6\16\6&\13\6\3"+
		"\7\3\7\3\7\3\7\7\7,\n\7\f\7\16\7/\13\7\3\7\3\7\3\b\3\b\3\b\6\b\66\n\b"+
		"\r\b\16\b\67\3\b\3\b\3\t\3\t\3\t\3\t\3\t\2\2\n\2\4\6\b\n\f\16\20\2\2:"+
		"\2\22\3\2\2\2\4\25\3\2\2\2\6\31\3\2\2\2\b\35\3\2\2\2\n$\3\2\2\2\f\'\3"+
		"\2\2\2\16\62\3\2\2\2\20;\3\2\2\2\22\23\5\n\6\2\23\24\7\2\2\3\24\3\3\2"+
		"\2\2\25\26\7\7\2\2\26\27\7\17\2\2\27\30\7\b\2\2\30\5\3\2\2\2\31\32\7\t"+
		"\2\2\32\33\7\17\2\2\33\34\7\n\2\2\34\7\3\2\2\2\35\36\7\r\2\2\36\37\7\17"+
		"\2\2\37 \7\16\2\2 \t\3\2\2\2!#\5\f\7\2\"!\3\2\2\2#&\3\2\2\2$\"\3\2\2\2"+
		"$%\3\2\2\2%\13\3\2\2\2&$\3\2\2\2\'(\7\3\2\2()\5\4\3\2)-\5\b\5\2*,\5\16"+
		"\b\2+*\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\60\3\2\2\2/-\3\2\2\2\60"+
		"\61\7\4\2\2\61\r\3\2\2\2\62\63\7\5\2\2\63\65\5\6\4\2\64\66\5\20\t\2\65"+
		"\64\3\2\2\2\66\67\3\2\2\2\67\65\3\2\2\2\678\3\2\2\289\3\2\2\29:\7\6\2"+
		"\2:\17\3\2\2\2;<\7\13\2\2<=\7\17\2\2=>\7\f\2\2>\21\3\2\2\2\5$-\67";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}